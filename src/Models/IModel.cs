namespace GiaDif.EntityFrameworkCore.Focaccia.Models
{
    /// <summary>
    /// The Model interface.
    /// </summary>
    public interface IModel
    {
        /// <summary>
        /// The id of the entity.
        /// </summary>
        int Id { get; set; }
    }
}