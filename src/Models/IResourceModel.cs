namespace GiaDif.EntityFrameworkCore.Focaccia.Models
{
    /// <summary>
    /// The Resource Model interface.
    /// </summary>
    public interface IResourceModel : IModel
    {
        /// <summary>
        /// The path of the resource the entity represents.
        /// </summary>
        string Path { get; set; }
    }
}