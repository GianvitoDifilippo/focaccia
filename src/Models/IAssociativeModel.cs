using System;

namespace GiaDif.EntityFrameworkCore.Focaccia.Models
{
    /// <summary>
    /// The Associative Model interface.
    /// </summary>
    [Obsolete]
    public interface IAssociativeModel
    {
        /// <summary>
        /// The first foreign key.
        /// </summary>
        int FirstId { get; set; }
        /// <summary>
        /// The second foreign key.
        /// </summary>
        int SecondId { get; set; }
    }
}