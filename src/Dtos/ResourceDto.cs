using GiaDif.EntityFrameworkCore.Focaccia.Models;

namespace GiaDif.EntityFrameworkCore.Focaccia.Dtos
{
    /// <summary>
    /// The base Resource Data Transfer Object class.
    /// Represents an entity which models a resource.
    /// </summary>
    public abstract class ResourceDto<RM> : Dto<RM>
        where RM: IResourceModel
    {
        /// <summary>
        /// The default empty constructor.
        /// </summary>
        public ResourceDto() : base() { }

        /// <summary>
        /// Constructs the Resource DTO from an Id and a Path.
        /// </summary>
        public ResourceDto(int id, string path)
            : base(id)
        {
            Path = path;
        }

        /// <summary>
        /// Constructs the Resource DTO from the corrisponding Resource Model.
        /// </summary>
        /// <param name="model">
        /// The resource model.
        /// </param>
        public ResourceDto(RM model) : base(model) { }

        /// <summary>
        /// The Path of the resource.
        /// </summary>
        public string Path { get; set; }
    }
}