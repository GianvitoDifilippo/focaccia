using GiaDif.EntityFrameworkCore.Focaccia.Models;
using Newtonsoft.Json;

namespace GiaDif.EntityFrameworkCore.Focaccia.Dtos
{
    /// <summary>
    /// The base Data Transfer Object class.
    /// Represents a generic entity with Id.
    /// </summary>
    public abstract class Dto<M> : IStringRepresentable
        where M: IModel
    {
        /// <summary>
        /// The default empty constructor.
        /// </summary>
        public Dto() { }

        /// <summary>
        /// Constructs the DTO from an Id.
        /// </summary>
        public Dto(int id)
        {
            Id = id;
        }

        /// <summary>
        /// Constructs the DTO from the corrisponding Model.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        public Dto(M model) 
        {
            FromModel(model);
        }

        /// <summary>
        /// The Id of the entity.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Specifies whether the object can be represented by a string.
        /// </summary>
        [JsonIgnore]
        public virtual bool UsesCompactRepresentation { get; set; }

        /// <summary>
        /// Updates the DTO from the associated model.
        /// </summary>
        public virtual void Update(M model)
        {
            Id = model.Id;
        }

        /// <summary>
        /// Generates the DTO from the corrisponding Model.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The DTO.
        /// </returns>
        public abstract Dto<M> FromModel(M model);
        
        /// <summary>
        /// Generates the Model from the corrisponding DTO.
        /// </summary>
        /// <returns>
        /// The model.
        /// </returns>
        public abstract M ToModel();
    }

    public static partial class ModelToDtoExtensions
    {
        /// <summary>
        /// Generates the DTO from the corrisponding Model.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The DTO.
        /// </returns>
        public static D ToDto<D, M>(this M model) where D: Dto<M>, new() where M: IModel
            => new D().FromModel(model) as D;
    }
}