using System;
using GiaDif.EntityFrameworkCore.Focaccia.Models;
using Newtonsoft.Json;

namespace GiaDif.EntityFrameworkCore.Focaccia.Dtos
{
    /// <summary>
    /// The base Associative Data Transfer Object class.
    /// Represents an associative entity which has two foreign keys to other entities.
    /// </summary>
    [Obsolete]
    public abstract class AssociativeDto<AM>: IStringRepresentable
        where AM: IAssociativeModel
    {
        /// <summary>
        /// The default empty constructor.
        /// </summary>
        public AssociativeDto() { }

        /// <summary>
        /// Constructs the DTO from the foreign keys.
        /// </summary>
        /// <param name="firstId">
        /// The foreign key to the first entity.
        /// </param>
        /// <param name="secondId">
        /// The foreign key to the second entity.
        /// </param>
        public AssociativeDto(int firstId, int secondId)
        {
            FirstId = firstId;
            SecondId = secondId;
        }

        /// <summary>
        /// Constructs the DTO from the corrisponding Associative Model.
        /// </summary>
        /// <param name="model">
        /// The associative model.
        /// </param>
        public AssociativeDto(AM model) 
        {
            FromAModel(model);
        }

        /// <summary>
        /// The Id of the first entity.
        /// </summary>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown when FirstId is set to a value less than or equal to 0.
        /// </exception>
        [JsonIgnore]
        public int FirstId { get; set;}

        /// <summary>
        /// The Id of the second entity.
        /// </summary>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown when SecondId is set to a value less than or equal to 0.
        /// </exception>
        [JsonIgnore]
        public int SecondId { get; set; }

        /// <summary>
        /// Specifies whether the object can be represented by a string.
        /// </summary>
        [JsonIgnore]
        public virtual bool UsesCompactRepresentation { get; set; }

        /// <summary>
        /// Updates the DTO from the associated model.
        /// </summary>
        public virtual void Update(AM model)
        {
            FirstId = model.FirstId;
            SecondId = model.SecondId;
        }

        /// <summary>
        /// Generates the Associative DTO from the corrisponding Associative Model.
        /// </summary>
        /// <param name="model">
        /// The associative model.
        /// </param>
        /// <returns>
        /// The associative DTO.
        /// </returns>
        public abstract AssociativeDto<AM> FromAModel(AM model);

        /// <summary>
        /// Generates the Associative Model from the corrisponding Associative DTO.
        /// </summary>
        /// <returns>
        /// The associative model.
        /// </returns>
        public abstract AM ToAModel();
    }

    /// <summary>
    /// Contains some Model extension methods.
    /// </summary>
    [Obsolete]
    public static partial class ModelToDtoExtensions
    {
        /// <summary>
        /// Generates the Associative DTO from the corrisponding Associative Model.
        /// </summary>
        /// <param name="model">
        /// The associative model.
        /// </param>
        /// <returns>
        /// The associative DTO.
        /// </returns>
        public static AD ToADto<AD, AM>(this AM model) where AD: AssociativeDto<AM>, new() where AM: IAssociativeModel
            => new AD().FromAModel(model) as AD;
    }
}