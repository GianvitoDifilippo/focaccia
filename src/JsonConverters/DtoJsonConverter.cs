using System;
using GiaDif.EntityFrameworkCore.Focaccia.Dtos;
using GiaDif.EntityFrameworkCore.Focaccia.Models;
using Newtonsoft.Json;

namespace GiaDif.EntityFrameworkCore.Focaccia
{
    /// <summary>
    /// A JsonCoverter for the Dto class.
    /// It overrides the reading of the JSON and the writing if the object being converted can be represented in a compact form.
    /// </summary>
    /// <typeparam name="D">
    /// A type that inherits from the Dto&lt;<typeparamref name="M"/>&gt; class.
    /// </typeparam>
    /// <typeparam name="M">
    /// A type that implements IModel.
    /// </typeparam>
    public class DtoJsonConverter<D, M> : SRJsonConverter<D>
        where D : Dto<M>, new()
        where M : IModel
    {
        /// <summary>
        /// Value indicating whether this Newtonsoft.Json.JsonConverter is reading the JSON.
        /// </summary>
        protected bool IsReading = true;

        /// <summary>
        /// Reads the JSON representation of the object. It reads the Id of the entity if the JSON object is an integer.
        /// </summary>
        /// <param name="reader">
        /// The Newtonsoft.Json.JsonReader to read from.
        /// </param>
        /// <param name="objectType">
        /// Type of the object.
        /// </param>
        /// <param name="existingValue">
        /// The existing value of object being read. If there is no existing value then null will be used.
        /// </param>
        /// <param name="hasExistingValue">
        /// The existing value has a value.
        /// </param>
        /// <param name="serializer">
        /// The calling serializer.
        /// </param>
        /// <returns>
        /// The object value.
        /// </returns>
        public override D ReadJson(JsonReader reader, Type objectType, D existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Integer) {
                return new D { Id = Convert.ToInt32(reader.Value) };
            }
            else {
                IsReading = false;
                D result = serializer.Deserialize<D>(reader);
                IsReading = true;
                return result;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this Newtonsoft.Json.JsonConverter can read JSON.
        /// </summary>
        public override bool CanRead { get => IsReading; }
    }
}