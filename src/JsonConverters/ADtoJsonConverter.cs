using System;
using GiaDif.EntityFrameworkCore.Focaccia.Dtos;
using GiaDif.EntityFrameworkCore.Focaccia.Models;
using Newtonsoft.Json;

namespace GiaDif.EntityFrameworkCore.Focaccia
{
    /// <summary>
    /// A JsonCoverter for the AssociativeDto class.
    /// It overrides the reading of the JSON and the writing if the object being converted can be represented in a compact form.
    /// </summary>
    /// <typeparam name="AD">
    /// A type that inherits from the AssociativeDto&lt;<typeparamref name="AM"/>&gt; class.
    /// </typeparam>
    /// <typeparam name="AM">
    /// A type that implements IAssociativeModel.
    /// </typeparam>
    [Obsolete]
    public class ADtoJsonConverter<AD, AM> : SRJsonConverter<AD>
        where AD : AssociativeDto<AM>, new()
        where AM : IAssociativeModel
    {
        /// <summary>
        /// Value indicating whether this Newtonsoft.Json.JsonConverter is reading the JSON.
        /// </summary>
        protected bool IsReading = true;
    
        /// <summary>
        /// Reads the JSON representation of the object. It tries to read two intergers if the JSON object is an array.
        /// </summary>
        /// <param name="reader">
        /// The Newtonsoft.Json.JsonReader to read from.
        /// </param>
        /// <param name="objectType">
        /// Type of the object.
        /// </param>
        /// <param name="existingValue">
        /// The existing value of object being read. If there is no existing value then null will be used.
        /// </param>
        /// <param name="hasExistingValue">
        /// The existing value has a value.
        /// </param>
        /// <param name="serializer">
        /// The calling serializer.
        /// </param>
        /// <returns>
        /// The object value.
        /// </returns>
        public override AD ReadJson(JsonReader reader, Type objectType, AD existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.StartArray) {
                int[] ids = serializer.Deserialize<int[]>(reader);
                AD result = new AD();
                result.FirstId = ids[0];
                result.SecondId = ids[1];
                return result;
            }
            else {
                IsReading = false;
                AD result = serializer.Deserialize<AD>(reader);
                IsReading = true;
                return result;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this Newtonsoft.Json.JsonConverter can read JSON.
        /// </summary>
        public override bool CanRead { get => IsReading; }
    }
}