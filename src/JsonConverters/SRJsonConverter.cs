using System;
using Newtonsoft.Json;

namespace GiaDif.EntityFrameworkCore.Focaccia
{
    /// <summary>
    /// A JsonCoverter for the IStringRepresentable interface.
    /// It overrides the writing of the JSON if the object being converted can be represented in a compact form.
    /// </summary>
    /// <typeparam name="T">
    /// A type that implements IStringRepresentable.
    /// </typeparam>
    public class SRJsonConverter<T> : JsonConverter<T>
        where T: IStringRepresentable
    {
        /// <summary>
        /// Value indicating whether this Newtonsoft.Json.JsonConverter is writing the JSON.
        /// </summary>
        protected bool IsWriting = true;

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <remarks>
        /// This method should not be called because this class is just a JSON writer.
        /// </remarks>
        /// <param name="reader">
        /// The Newtonsoft.Json.JsonReader to read from.
        /// </param>
        /// <param name="objectType">
        /// Type of the object.
        /// </param>
        /// <param name="existingValue">
        /// The existing value of object being read. If there is no existing value then null will be used.
        /// </param>
        /// <param name="hasExistingValue">
        /// The existing value has a value.
        /// </param>
        /// <param name="serializer">
        /// The calling serializer.
        /// </param>
        /// <returns>
        /// The object value.
        /// </returns>
        public override T ReadJson(JsonReader reader, Type objectType, T existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes the JSON representation of the object. It writes the compact representation of the object if its
        /// UsesCompactRepresentation flag is set to True.
        /// </summary>
        /// <param name="writer">
        /// The Newtonsoft.Json.JsonWriter to write to.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="serializer">
        /// The calling serializer.
        /// </param>
        public override void WriteJson(JsonWriter writer, T value, JsonSerializer serializer)
        {
            if (value.UsesCompactRepresentation) {
                writer.WriteValue(value.ToString());
            }
            else {
                IsWriting = false;
                serializer.Serialize(writer, value);
                IsWriting = true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this Newtonsoft.Json.JsonConverter can read JSON.
        /// </summary>
        public override bool CanRead => false;
        /// <summary>
        /// Gets a value indicating whether this Newtonsoft.Json.JsonConverter can write JSON.
        /// </summary>
        public override bool CanWrite { get => IsWriting; }
    }
}