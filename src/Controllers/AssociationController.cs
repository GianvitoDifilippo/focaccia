using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GiaDif.EntityFrameworkCore.Focaccia.Dtos;
using GiaDif.EntityFrameworkCore.Focaccia.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GiaDif.EntityFrameworkCore.Focaccia.Controllers
{
    /// <summary>
    /// A Restful Controller implementation for an Association DTO object.
    /// Provides an implementation of the Get, Put, Post and Delete endpoints, as well with and additional GetAll endpoint.
    /// </summary>
    /// <typeparam name="C">
    /// A type that inherits from the DbContext class.
    /// </typeparam>
    /// <typeparam name="AD">
    /// A type that inherits from the AssociativeDto&lt;<typeparamref name="AM"/>&gt; class.
    /// </typeparam>
    /// <typeparam name="AM">
    /// A type that implements IAssociativeModel.
    /// </typeparam>
    [Obsolete]
    public abstract class AssociationController<C, AD, AM> : ControllerBase
        where C: DbContext
        where AD: AssociativeDto<AM>, new()
        where AM: class, IAssociativeModel
    {
        /// <summary>
        /// The DbContext of the controller.
        /// </summary>
        protected readonly C Context;
        /// <summary>
        /// The DbSet of the controller.
        /// </summary>
        protected readonly DbSet<AM> DbSet;
        /// <summary>
        /// The default target for queries.
        /// </summary>
        protected readonly IQueryable<AM> DefaultTarget;
        /// <summary>
        /// The name of the controller.
        /// </summary>
        protected readonly string ControllerName;

        /// <summary>
        /// Constructs the Controller from the DbContext, the DbSet and, optionally, the controller name.
        /// </summary>
        /// <param name="context">
        /// The context of the controller.
        /// </param>
        /// <param name="dbSet">
        /// The database of the controller.
        /// </param>
        /// <param name="controllerName">
        /// The name of the controller. If not specified, the constructor will try to extract it from the class name.
        /// </param>
        public AssociationController(C context, DbSet<AM> dbSet, string controllerName = null)
        {
            Context = context;
            DbSet = dbSet;
            DefaultTarget = CreateDefaultTarget(context);
            ControllerName = controllerName ?? (this.GetType().Name.Contains("Controller") ? this.GetType().Name.Replace("Controller", "") : "");
        }

        /// <summary>
        /// Creates the default target for queries.
        /// </summary>
        /// <param name="context">
        /// The context of the controller.
        /// </param>
        /// <returns>
        /// The default target.
        /// </returns>
        protected virtual IQueryable<AM> CreateDefaultTarget(C context)
        {
            return DbSet;
        }

        /// <summary>
        /// Gets all the entities in the specified or default target.
        /// </summary>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <param name="compactRepresentation">
        /// Specifies wheter to use the compact representation in the JSON representation of the object.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> GetAll(IQueryable<AM> target = null, bool compactRepresentation = false)
        {
            if (target == null) target = DefaultTarget;
            List<AM> models = await target.ToListAsync();
            
            try {
                IEnumerable<AD> adtos = models.Select(model => {
                    AD adto = model.ToADto<AD, AM>();
                    adto.UsesCompactRepresentation = compactRepresentation;
                    return adto;
                });
                return Ok(adtos);
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(AD).Name + ".FromModel implementation"));
            }
        }

        /// <summary>
        /// Gets the entity with the specified ids.
        /// </summary>
        /// <param name="firstId">
        /// The first foreign key of the entity.
        /// </param>
        /// <param name="secondId">
        /// The second foreign key of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <param name="compactRepresentation">
        /// Specifies wheter to use the compact representation in the JSON representation of the object.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Get(int firstId, int secondId, IQueryable<AM> target = null, bool compactRepresentation = false)
        {
            if (target == null) target = DefaultTarget;
            AM model = await target
                .Where(x => x.FirstId == firstId && x.SecondId == secondId)
                .FirstOrDefaultAsync();
            if (model == null) {
                return NotFound();
            }

            try {
                AD adto = model.ToADto<AD, AM>();
                adto.UsesCompactRepresentation = compactRepresentation;
                return Ok(adto);
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(AD).Name + ".FromModel implementation"));
            }
        }

        /// <summary>
        /// Puts the entity with the specified ids.
        /// </summary>
        /// <param name="firstId">
        /// The first foreign key of the entity.
        /// </param>
        /// <param name="secondId">
        /// The second foreign key of the entity.
        /// </param>
        /// <param name="dto">
        /// The DTO of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Put(int firstId, int secondId, AD dto, IQueryable<AM> target = null)
        {
            if (target == null) target = DefaultTarget;
            if (dto.FirstId != firstId) {
                return BadRequest("The FirstId of the DTO must be equal to the first id of the request");
            }
            if (dto.SecondId != secondId) {
                return BadRequest("The SecondId of the DTO must be equal to the second id of the request");
            }
            if (!EntryExists(firstId, secondId, target)) {
                return NotFound();
            }

            try {
                await OnPutPreprocess(firstId, secondId, dto);
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPutPreprocess implementation"));
            }
            try {
                var validationResult = await OnPutValidate(firstId, secondId, dto);
                if (validationResult != null && validationResult.Any()) {
                    return BadRequest(validationResult.ToArray());
                }
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPutValidate implementation"));
            }

            using (var transaction = await Context.Database.BeginTransactionAsync()) {
                AM model = null;
                try {
                    model = dto.ToAModel();
                } catch (Exception e) {
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(AD).Name + ".ToModel implementation"));
                }

                try {
                    Context.Entry(model).State = EntityState.Modified;
                    await Context.SaveChangesAsync();
                    await transaction.CommitAsync();
                } catch (DbUpdateConcurrencyException e) {
                    await transaction.RollbackAsync();
                    return StatusCode(412, e);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return BadRequest(e);
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Posts the entity to the database.
        /// </summary>
        /// <param name="dto">
        /// The DTO of the entity.
        /// </param>
        /// <param name="actionName">
        /// The name of the action to use for generating the URL.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Post(AD dto, string actionName = null)
        {
            try {
                await OnPostPreprocess(dto);
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPostPreprocess implementation"));
            }
            try {
                var validationResult = await OnPostValidate(dto);
                if (validationResult != null && validationResult.Any()) {
                    return BadRequest(validationResult.ToArray());
                }
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPostValidate implementation"));
            }

            using (var transaction = await Context.Database.BeginTransactionAsync()) {
                AM model = null;
                try {
                    model = dto.ToAModel();
                } catch (Exception e) {
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(AD).Name + ".ToModel implementation"));
                }

                try {
                    DbSet.Add(model);
                    await Context.SaveChangesAsync();
                } catch (DbUpdateConcurrencyException e) {
                    await transaction.RollbackAsync();
                    return StatusCode(412, e);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return BadRequest(e);
                }

                try {
                    dto.Update(model);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(AD).Name + ".Update implementation"));
                }
                await transaction.CommitAsync();
            }
            return CreatedAtAction(actionName ?? "Get" + ControllerName, new { firstId = dto.FirstId, secondId = dto.SecondId }, dto);
        }

        /// <summary>
        /// Deletes the entity with the specified id.
        /// </summary>
        /// <param name="firstId">
        /// The first foreign key of the entity.
        /// </param>
        /// <param name="secondId">
        /// The second foreign key of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Delete(int firstId, int secondId, IQueryable<AM> target = null)
        {
            if (target == null) target = DefaultTarget;
            AM model = await target
                .Where(x => x.FirstId == firstId && x.SecondId == secondId)
                .FirstOrDefaultAsync();
            if (model == null) {
                return NotFound();
            }

            try {
                DbSet.Remove(model);
                await Context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException e) {
                return StatusCode(412, e);
            } catch (Exception e) {
                return StatusCode(500, e);
            }

            try {
                return Ok(model.ToADto<AD, AM>());
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(AD).Name + ".FromModel implementation"));
            }
        }

        /// <summary>
        /// Tells if an entity exists in the database.
        /// </summary>
        /// <param name="firstId">
        /// The first foreign key of the entity.
        /// </param>
        /// <param name="secondId">
        /// The second foreign key of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <returns>
        /// A value that indicates if the entity exists in the database.
        /// </returns>
        protected bool EntryExists(int firstId, int secondId, IQueryable<AM> target)
            => target.Any(e => e.FirstId == firstId && e.SecondId == secondId);

        /// <summary>
        /// Preprocess the object on PUT requests.
        /// </summary>
        /// <param name="firstId">
        /// The first foreign key of the object.
        /// </param>
        /// <param name="secondId">
        /// The second foreign key of the object.
        /// </param>
        /// <param name="dto">
        /// The object to be preprocessed.
        /// </param>
        protected virtual async Task OnPutPreprocess(int firstId, int secondId, AD dto) => await Task.CompletedTask;

        /// <summary>
        /// Preprocess the object on POST requests.
        /// </summary>
        /// <param name="dto">
        /// The object to be preprocessed.
        /// </param>
        protected virtual async Task OnPostPreprocess(AD dto) => await Task.CompletedTask;

        /// <summary>
        /// Validates the object on PUT requests.
        /// </summary>
        /// <param name="firstId">
        /// The first foreign key of the object.
        /// </param>
        /// <param name="secondId">
        /// The second foreign key of the object.
        /// </param>
        /// <param name="dto">
        /// The object to be validated.
        /// </param>
        /// <returns>
        /// A List of validation errors.
        /// </returns>
        protected virtual async Task<List<string>> OnPutValidate(int firstId, int secondId, AD dto)
        {
            List<string> errors = new List<string>();
            if (dto.FirstId <= 0) {
                errors.Add("The first foreign key of the object must be greater than zero");
            }
            if (dto.SecondId <= 0) {
                errors.Add("The second foreign key of the object must be greater than zero");
            }
            return await Task.FromResult(errors);
        }

        /// <summary>
        /// Validates the object on POST requests.
        /// </summary>
        /// <param name="dto">
        /// The object to be validated.
        /// </param>
        /// <returns>
        /// A List of validation errors.
        /// </returns>
        protected virtual async Task<List<string>> OnPostValidate(AD dto) => await Task.FromResult<List<string>>(null);
    }
}