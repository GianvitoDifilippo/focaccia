using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GiaDif.EntityFrameworkCore.Focaccia.Dtos;
using GiaDif.EntityFrameworkCore.Focaccia.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GiaDif.EntityFrameworkCore.Focaccia.Controllers
{
    /// <summary>
    /// A Restful Controller implementation for a DTO object.
    /// Provides an implementation of the Get, Put, Post and Delete endpoints, as well with and additional GetAll endpoint.
    /// </summary>
    /// <typeparam name="C">
    /// A type that inherits from the DbContext class.
    /// </typeparam>
    /// <typeparam name="D">
    /// A type that inherits from the Dto&lt;<typeparamref name="M"/>&gt; class.
    /// </typeparam>
    /// <typeparam name="M">
    /// A type that implements IModel.
    /// </typeparam>
    public abstract class RestfulController<C, D, M> : ControllerBase
        where C: DbContext
        where D: Dto<M>, new()
        where M: class, IModel
    {
        /// <summary>
        /// The DbContext of the controller.
        /// </summary>
        protected readonly C Context;
        /// <summary>
        /// The DbSet of the controller.
        /// </summary>
        protected readonly DbSet<M> DbSet;
        /// <summary>
        /// The default target for queries.
        /// </summary>
        protected readonly IQueryable<M> DefaultTarget;
        /// <summary>
        /// The name of the controller.
        /// </summary>
        protected readonly string ControllerName;

        /// <summary>
        /// Constructs the Controller from the DbContext, the DbSet and, optionally, the controller name.
        /// </summary>
        /// <param name="context">
        /// The context of the controller.
        /// </param>
        /// <param name="dbSet">
        /// The database of the controller.
        /// </param>
        /// <param name="controllerName">
        /// The name of the controller. If not specified, the constructor will try to extract it from the class name.
        /// </param>
        public RestfulController(C context, DbSet<M> dbSet, string controllerName = null)
        {
            Context = context;
            DbSet = dbSet;
            DefaultTarget = CreateDefaultTarget(context);
            ControllerName = controllerName ?? (this.GetType().Name.Contains("Controller") ? this.GetType().Name.Replace("Controller", "") : "");
        }

        /// <summary>
        /// Creates the default target for queries.
        /// </summary>
        /// <param name="context">
        /// The context of the controller.
        /// </param>
        /// <returns>
        /// The default target.
        /// </returns>
        protected virtual IQueryable<M> CreateDefaultTarget(C context)
        {
            return DbSet;
        }

        /// <summary>
        /// Gets all the entities in the specified or default target.
        /// </summary>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <param name="compactRepresentation">
        /// Specifies wheter to use the compact representation in the JSON representation of the object.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> GetAll(IQueryable<M> target = null, bool compactRepresentation = false)
        {
            if (target == null) target = DefaultTarget;
            List<M> models = await target.ToListAsync();
            
            try {
                IEnumerable<D> dtos = models.Select(model => {
                    D dto = model.ToDto<D, M>();
                    dto.UsesCompactRepresentation = compactRepresentation;
                    return dto;
                });
                return Ok(dtos);
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(D).Name + ".FromModel implementation"));
            }
        }

        /// <summary>
        /// Gets the entity with the specified id.
        /// </summary>
        /// <param name="id">
        /// The Id of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <param name="compactRepresentation">
        /// Specifies wheter to use the compact representation in the JSON representation of the object.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Get(int id, IQueryable<M> target = null, bool compactRepresentation = false)
        {
            if (target == null) target = DefaultTarget;
            M model = await target
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            if (model == null) {
                return NotFound();
            }

            try {
                D dto = model.ToDto<D, M>();
                dto.UsesCompactRepresentation = compactRepresentation;
                return Ok(dto);
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(D).Name + ".FromModel implementation"));
            }
        }

        /// <summary>
        /// Puts the entity with the specified id.
        /// </summary>
        /// <param name="id">
        /// The Id of the entity.
        /// </param>
        /// <param name="dto">
        /// The DTO of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Put(int id, D dto, IQueryable<M> target = null)
        {
            if (target == null) target = DefaultTarget;
            if (dto.Id != id) {
                return BadRequest("The Id of the DTO must be equal to the id of the request");
            }
            try {
                var validationResult = await OnPutValidate(id, dto);
                if (validationResult != null && validationResult.Any()) {
                    return BadRequest(validationResult.ToArray());
                }
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPutValidate implementation"));
            }
            
            if (!EntryExists(id, target)) {
                return NotFound();
            }

            using (var transaction = await Context.Database.BeginTransactionAsync()) {
                M model = null;
                try {
                    model = dto.ToModel();
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(D).Name + ".ToModel implementation"));
                }
                try {
                    await OnPutPreprocess(id, dto, model);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPutPreprocess implementation"));
                }

                try {
                    Context.Entry(model).State = EntityState.Modified;
                    await Context.SaveChangesAsync();
                    await transaction.CommitAsync();
                } catch (DbUpdateConcurrencyException e) {
                    await transaction.RollbackAsync();
                    return StatusCode(412, e);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return BadRequest(e);
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Posts the entity to the database.
        /// </summary>
        /// <param name="dto">
        /// The DTO of the entity.
        /// </param>
        /// <param name="actionName">
        /// The name of the action to use for generating the URL.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Post(D dto, string actionName = null)
        {
            try {
                var validationResult = await OnPostValidate(dto);
                if (validationResult != null && validationResult.Any()) {
                    return BadRequest(validationResult.ToArray());
                }
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPostValidate implementation"));
            }

            using (var transaction = await Context.Database.BeginTransactionAsync()) {
                M model = null;
                try {
                    model = dto.ToModel();
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(D).Name + ".ToModel implementation"));
                }
                try {
                    await OnPostPreprocess(dto, model);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPostPreprocess implementation"));
                }

                try {
                    DbSet.Add(model);
                    await Context.SaveChangesAsync();
                } catch (DbUpdateConcurrencyException e) {
                    await transaction.RollbackAsync();
                    return StatusCode(412, e);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return BadRequest(e);
                }

                try {
                    dto.Update(model);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(D).Name + ".Update implementation"));
                }
                await transaction.CommitAsync();
            }
            return CreatedAtAction(actionName ?? "Get" + ControllerName, new { id = dto.Id }, dto);
        }

        /// <summary>
        /// Deletes the entity with the specified id.
        /// </summary>
        /// <param name="id">
        /// The Id of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Delete(int id, IQueryable<M> target = null)
        {
            if (target == null) target = DefaultTarget;
            M model = await target
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            if (model == null) {
                return NotFound();
            }

            try {
                DbSet.Remove(model);
                await Context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException e) {
                return StatusCode(412, e);
            } catch (Exception e) {
                return StatusCode(500, e);
            }

            try {
                return Ok(model.ToDto<D, M>());
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(D).Name + ".FromModel implementation"));
            }
        }

        /// <summary>
        /// Tells if an entity exists in the database.
        /// </summary>
        /// <param name="id">
        /// The Id of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <returns>
        /// A value that indicates if the entity exists in the database.
        /// </returns>
        protected bool EntryExists(int id, IQueryable<M> target) => target.Any(e => e.Id == id);

        /// <summary>
        /// Preprocess the object on PUT requests.
        /// </summary>
        /// <param name="id">
        /// The id of the object.
        /// </param>
        /// <param name="dto">
        /// The object to be preprocessed.
        /// </param>
        /// <param name="model">
        /// The model to be preprocessed.
        /// </param>
        protected virtual Task OnPutPreprocess(int id, D dto, M model) => Task.CompletedTask;

        /// <summary>
        /// Preprocess the object on POST requests.
        /// </summary>
        /// <param name="dto">
        /// The object to be preprocessed.
        /// </param>
        /// <param name="model">
        /// The model to be preprocessed.
        /// </param>
        protected virtual Task OnPostPreprocess(D dto, M model) => Task.CompletedTask;

        /// <summary>
        /// Validates the object on PUT requests.
        /// </summary>
        /// <param name="id">
        /// The id of the object.
        /// </param>
        /// <param name="dto">
        /// The object to be validated.
        /// </param>
        /// <returns>
        /// A List of validation errors.
        /// </returns>
        protected virtual Task<List<string>> OnPutValidate(int id, D dto)
        {
            List<string> errors = new List<string>();
            if (dto.Id <= 0) {
                errors.Add("The Id of the object must be greater than zero");
            }
            return Task.FromResult(errors);
        }

        /// <summary>
        /// Validates the object on POST requests.
        /// </summary>
        /// <param name="dto">
        /// The object to be validated.
        /// </param>
        /// <returns>
        /// A List of validation errors.
        /// </returns>
        protected virtual Task<List<string>> OnPostValidate(D dto) => Task.FromResult<List<string>>(null);
    }
}