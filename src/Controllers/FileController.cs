using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MimeTypes;

namespace GiaDif.EntityFrameworkCore.Focaccia.Controllers
{
    /// <summary>
    /// A file transfer system that can Get, Post and Delete files.
    /// </summary>
    public class FileController : ControllerBase
    {
        /// <summary>
        /// The folder manager of the folder this controller is operating into.
        /// </summary>
        protected FolderManager Folder;

        /// <summary>
        /// The name of the controller.
        /// </summary>
        protected readonly string ControllerName;

        /// <summary>
        /// Constructs the Controller from the file folder path.
        /// </summary>
        /// <param name="fileFolderPath">
        /// The path of the folder this controller is operating into, relative to the project root folder.
        /// </param>
        /// <param name="controllerName">
        /// The name of the controller. If not specified, the constructor will try to extract it from the class name.
        /// </param>
        public FileController(string fileFolderPath, string controllerName = null)
        {
            Folder = new FolderManager(fileFolderPath);
            ControllerName = controllerName ?? (this.GetType().Name.Contains("Controller") ? this.GetType().Name.Replace("Controller", "") : "");
        }

        /// <summary>
        /// Gets the file with the specified name.
        /// </summary>
        /// <param name="fileName">
        /// The name of the file.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        public virtual IActionResult GetFile(string fileName)
        {
            fileName = HttpUtility.UrlDecode(fileName);
            FileStream file = Folder.ReadFile(fileName);
            if (file == null) {
                return NotFound();
            }
            return File(file, MimeTypeMap.GetMimeType(System.IO.Path.GetExtension(fileName).ToLower()));
        }

        /// <summary>
        /// Posts the file to the folder.
        /// </summary>
        /// <param name="body">
        /// The Microsoft.AspNetCore.Http.IFormCollection containing the file.
        /// </param>
        /// <param name="actionName">
        /// The name of the action to use for generating the URL.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        public virtual async Task<IActionResult> PostFile(IFormCollection body, string actionName = null)
        {
            IFormFileCollection files = body.Files;
            
            IFormFile file = files.FirstOrDefault();
            string fileName = file.Name;
            try {
                if (!await Folder.WriteFile(file, fileName)) {
                    return BadRequest("The file already exists");
                }
            } catch (Exception e) {
                return StatusCode(500, e);
            }
            return CreatedAtAction(actionName ?? "Get" + ControllerName, new { path = fileName }, null);
        }

        /// <summary>
        /// Deletes the file with the specified name.
        /// </summary>
        /// <param name="fileName">
        /// The name of the file.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        public virtual IActionResult DeleteFile(string fileName)
        {
            if (Folder.DeleteFile(fileName)) {
                return Ok();
            }
            return NotFound();
        }

        /// <summary>
        /// Validates the object on POST requests.
        /// </summary>
        /// <param name="body">
        /// The object to be validated.
        /// </param>
        /// <returns>
        /// A List of validation errors.
        /// </returns>
        protected virtual async Task<List<string>> OnPostFileValidate(IFormCollection body)
        {
            List<string> errors = new List<string>();
            if (!body.Files.Any()) {
                errors.Add("The request must include a file");
            }
            return await Task.FromResult(errors);
        }
    }
}