using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GiaDif.EntityFrameworkCore.Focaccia.Dtos;
using GiaDif.EntityFrameworkCore.Focaccia.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeTypes;

namespace GiaDif.EntityFrameworkCore.Focaccia.Controllers
{
    /// <summary>
    /// A Restful Controller implementation for a Resource DTO object.
    /// Provides the additional GetResource, PostResource and DeleteResource endpoints to the Restful implementation.
    /// </summary>
    /// <typeparam name="C">
    /// A type that inherits from the DbContext class.
    /// </typeparam>
    /// <typeparam name="RD">
    /// A type that inherits from the ResourceDto&lt;<typeparamref name="RM"/>&gt; class.
    /// </typeparam>
    /// <typeparam name="RM">
    /// A type that impements IResourceModel.
    /// </typeparam>
    public abstract class ResourceController<C, RD, RM> : ControllerBase
        where C: DbContext
        where RD: ResourceDto<RM>, new()
        where RM: class, IResourceModel, new()
    {
        /// <summary>
        /// The DbContext of the controller.
        /// </summary>
        protected readonly C Context;
        /// <summary>
        /// The DbSet of the controller.
        /// </summary>
        protected readonly DbSet<RM> DbSet;
        /// <summary>
        /// The default target for queries.
        /// </summary>
        protected readonly IQueryable<RM> DefaultTarget;
        /// <summary>
        /// The name of the controller.
        /// </summary>
        protected readonly string ControllerName;
        /// <summary>
        /// The folder manager of the folder this controller is operating into.
        /// </summary>
        protected FolderManager Folder;

        /// <summary>
        /// Constructs the Controller from the DbContext, the DbSet, the folder path and, optionally, the controller name.
        /// </summary>
        /// <param name="context">
        /// The context of the controller.
        /// </param>
        /// <param name="dbSet">
        /// The database of the controller.
        /// </param>
        /// <param name="folderPath">
        /// The path of the folder this controller is operating into, relative to the project root folder.
        /// </param>
        /// <param name="controllerName">
        /// The name of the controller. If not specified, the constructor will try to extract it from the class name.
        /// </param>
        public ResourceController(C context, DbSet<RM> dbSet, string folderPath, string controllerName = null)
        {
            Context = context;
            DbSet = dbSet;
            DefaultTarget = CreateDefaultTarget(context);
            ControllerName = controllerName ?? (this.GetType().Name.Contains("Controller") ? this.GetType().Name.Replace("Controller", "") : "");
            Folder = new FolderManager(folderPath);
        }

        /// <summary>
        /// Creates the default target for queries.
        /// </summary>
        /// <param name="context">
        /// The context of the controller.
        /// </param>
        /// <returns>
        /// The default target.
        /// </returns>
        protected virtual IQueryable<RM> CreateDefaultTarget(C context) => DbSet;

        /// <summary>
        /// Gets all the entities in the specified or default target.
        /// </summary>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <param name="compactRepresentation">
        /// Specifies wheter to use the compact representation in the JSON representation of the object.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> GetAll(IQueryable<RM> target = null, bool compactRepresentation = false)
        {
            if (target == null) target = DefaultTarget;
            List<RM> models = await target.ToListAsync();
            
            try {
                IEnumerable<RD> dtos = models.Select(model => {
                    RD dto = model.ToDto<RD, RM>();
                    dto.UsesCompactRepresentation = compactRepresentation;
                    return dto;
                });
                return Ok(dtos);
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(RD).Name + ".FromModel implementation"));
            }
        }

        /// <summary>
        /// Gets the entity with the specified id.
        /// </summary>
        /// <param name="id">
        /// The Id of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <param name="compactRepresentation">
        /// Specifies wheter to use the compact representation in the JSON representation of the object.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Get(int id, IQueryable<RM> target = null, bool compactRepresentation = false)
        {
            if (target == null) target = DefaultTarget;
            RM model = await target
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            if (model == null) {
                return NotFound();
            }

            try {
                RD dto = model.ToDto<RD, RM>();
                dto.UsesCompactRepresentation = compactRepresentation;
                return Ok(dto);
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(RD).Name + ".FromModel implementation"));
            }
        }

        /// <summary>
        /// Gets the file of the resource with the specified id.
        /// </summary>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> GetFile(int id)
        {
            RM model = await DbSet.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (model == null) {
                return NotFound();
            }
            FileStream file = Folder.ReadFile(model.Path);
            if (file == null) {
                return NotFound();
            }
            return File(file, MimeTypeMap.GetMimeType(System.IO.Path.GetExtension(model.Path).ToLower()));
        }

        /// <summary>
        /// Puts the entity with the specified id.
        /// </summary>
        /// <param name="id">
        /// The Id of the entity.
        /// </param>
        /// <param name="dto">
        /// The DTO of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Put(int id, RD dto, IQueryable<RM> target = null)
        {
            if (target == null) target = DefaultTarget;
            if (dto.Id != id) {
                return BadRequest("The Id of the DTO must be equal to the id of the request");
            }
            try {
                var validationResult = await OnPutValidate(id, dto);
                if (validationResult != null && validationResult.Any()) {
                    return BadRequest(validationResult.ToArray());
                }
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPutValidate implementation"));
            }
            
            if (!EntryExists(id, target)) {
                return NotFound();
            }

            using (var transaction = await Context.Database.BeginTransactionAsync()) {
                RM model = null;
                try {
                    model = dto.ToModel();
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(RD).Name + ".ToModel implementation"));
                }
                try {
                    await OnPutPreprocess(id, dto, model);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPutPreprocess implementation"));
                }

                try {
                    Context.Entry(model).State = EntityState.Modified;
                    await Context.SaveChangesAsync();
                    await transaction.CommitAsync();
                } catch (DbUpdateConcurrencyException e) {
                    await transaction.RollbackAsync();
                    return StatusCode(412, e);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return BadRequest(e);
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Posts the entity to the database and uploads the file to the folder.
        /// </summary>
        /// <param name="body">
        /// The body of the request.
        /// </param>
        /// <param name="dto">
        /// The DTO of the entity.
        /// </param>
        /// <param name="actionName">
        /// The name of the Get method of the controller.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> Post(IFormCollection body, RD dto, string actionName = null)
        {
            try {
                var validationResult = await OnPostValidate(dto);
                if (validationResult != null && validationResult.Any()) {
                    return BadRequest(validationResult.ToArray());
                }
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPostValidate implementation"));
            }

            using (var transaction = await Context.Database.BeginTransactionAsync()) {
                IFormFileCollection files = body.Files;
                if (!files.Any()) {
                    await transaction.RollbackAsync();
                    return BadRequest("The request must include a file");
                }
                IFormFile file = files.FirstOrDefault();
                dto.Path = file.Name;
                
                RM model = null;
                try {
                    model = dto.ToModel();
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(RD).Name + ".ToModel implementation"));
                }
                try {
                    await OnPostPreprocess(body, dto, model);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + this.GetType().Name + ".OnPutPreprocess implementation"));
                }

                try {
                    DbSet.Add(model);
                    await Context.SaveChangesAsync();
                } catch (DbUpdateConcurrencyException e) {
                    await transaction.RollbackAsync();
                    return StatusCode(412, e);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return BadRequest(e);
                }

                try {
                    dto.Update(model);
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(RD).Name + ".Update implementation"));
                }

                try {
                    if (!await Folder.WriteFile(file, dto.Path)) {
                        return BadRequest("The file already exists");
                    }
                } catch (Exception e) {
                    await transaction.RollbackAsync();
                    return StatusCode(500, e);
                }
                await transaction.CommitAsync();
            }
            return CreatedAtAction(actionName ?? "Get" + ControllerName, new { id = dto.Id }, dto);
        }

        /// <summary>
        /// Deletes the entity with the specified id and the file.
        /// </summary>
        /// <param name="id">
        /// The Id of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <returns>
        /// The result of the action.
        /// </returns>
        protected virtual async Task<IActionResult> DeleteResource(int id, IQueryable<RM> target = null)
        {
            if (target == null) target = DefaultTarget;
            RM model = await target
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            if (model == null) {
                return NotFound();
            }

            try {
                DbSet.Remove(model);
                await Context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException e) {
                return StatusCode(412, e);
            } catch (Exception e) {
                return StatusCode(500, e);
            }

            Folder.DeleteFile(model.Path);
            
            try {
                return Ok(model.ToDto<RD, RM>());
            } catch (Exception e) {
                return StatusCode(500, Focaccia.WrapForDebugging(e, "Check " + typeof(RD).Name + ".FromModel implementation"));
            }
        }

        /// <summary>
        /// Tells if an entity exists in the database.
        /// </summary>
        /// <param name="id">
        /// The Id of the entity.
        /// </param>
        /// <param name="target">
        /// The target of the query.
        /// </param>
        /// <returns>
        /// A value that indicates if the entity exists in the database.
        /// </returns>
        protected bool EntryExists(int id, IQueryable<RM> target) => target.Any(e => e.Id == id);

        /// <summary>
        /// Preprocess the object on PUT requests.
        /// </summary>
        /// <param name="id">
        /// The id of the object.
        /// </param>
        /// <param name="dto">
        /// The object to be preprocessed.
        /// </param>
        /// <param name="model">
        /// The model to be preprocessed.
        /// </param>
        protected virtual Task OnPutPreprocess(int id, RD dto, RM model) => Task.CompletedTask;

        /// <summary>
        /// Preprocess the object on POST requests.
        /// </summary>
        /// <param name="body">
        /// The body of the request.
        /// </param>
        /// <param name="dto">
        /// The object to be preprocessed.
        /// </param>
        /// <param name="model">
        /// The model to be preprocessed.
        /// </param>
        protected virtual Task OnPostPreprocess(IFormCollection body, RD dto, RM model) => Task.CompletedTask;

        /// <summary>
        /// Validates the object on PUT requests.
        /// </summary>
        /// <param name="id">
        /// The id of the object.
        /// </param>
        /// <param name="dto">
        /// The object to be validated.
        /// </param>
        /// <returns>
        /// A List of validation errors.
        /// </returns>
        protected virtual Task<List<string>> OnPutValidate(int id, RD dto)
        {
            List<string> errors = new List<string>();
            if (dto.Id <= 0) {
                errors.Add("The Id of the object must be greater than zero");
            }
            return Task.FromResult(errors);
        }

        /// <summary>
        /// Validates the object on PUT requests.
        /// </summary>
        /// <param name="dto">
        /// The object to be validated.
        /// </param>
        /// <returns>
        /// A List of validation errors.
        /// </returns>
        protected virtual Task<List<string>> OnPostValidate(RD dto) => Task.FromResult<List<string>>(null);
    }
}