using System;

namespace GiaDif.EntityFrameworkCore.Focaccia
{
    /// <summary>
    /// The IStringRepresentable interface.
    /// Represents an object that can be serialized with a compact string representation.
    /// </summary>
    public interface IStringRepresentable
    {
        /// <summary>
        /// Specifies whether the object can be represented by a string.
        /// </summary>
        bool UsesCompactRepresentation { get; set; }
    }
    
    /// <summary>
    /// The Utils class.
    /// Contains some global properties and methods.
    /// </summary>
    public static class Focaccia
    {
        /// <summary>
        /// If set to True, the controllers will wrap the exceptions within an exception showing where the problem might be coming.
        /// </summary>
        public static bool DebugExceptions { get; set; }

        /// <summary>
        /// Wraps the exception with a debug message.
        /// </summary>
        /// <returns>
        /// The wrapping exception.
        /// </returns>
        public static Exception WrapForDebugging(Exception e, string message)
        {
            return DebugExceptions ? new Exception(message, e) : e;
        }

    }
}