using System.IO;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;

namespace GiaDif.EntityFrameworkCore.Focaccia
{
    /// <summary>
    /// Reads, writes and deletes files from a folder.
    /// </summary>
    public class FolderManager
    {
        private DirectoryInfo _fileDirectory;

        /// <summary>
        /// Constructs the object from the folder path.
        /// </summary>
        public FolderManager(string folderPath)
        {
            FolderPath = folderPath;
        }

        /// <summary>
        /// The folder path.
        /// </summary>
        public string FolderPath {
            get => _fileDirectory.FullName;
            set => _fileDirectory = Directory.CreateDirectory(value);
        }

        private string ToUri(string fileName) => Path.Combine(FolderPath, fileName.Replace("/", "\\"));

        /// <summary>
        /// Opens an existing file.
        /// </summary>
        /// <returns>
        /// A read-only System.IO.FileStream on the specified path.
        /// </returns>
        public FileStream ReadFile(string fileName)
        {
            string uri = ToUri(fileName);
            uri = HttpUtility.UrlDecode(uri);
            return System.IO.File.Exists(uri) ? System.IO.File.OpenRead(uri) : null;
        }

        /// <summary>
        /// Writes to a non-existing file.
        /// </summary>
        /// <returns>
        /// True if the action succeeds, false if the file already exists.
        /// </returns>
        public async Task<bool> WriteFile(IFormFile file, string fileName)
        {
            string uri = ToUri(fileName);
            new System.IO.FileInfo(uri).Directory.Create();
            if (System.IO.File.Exists(uri)) return false;
            using (var stream = System.IO.File.Create(uri)) {
                await file.CopyToAsync(stream);
            }
            return true;
        }

        /// <summary>
        /// Deletes the specified file.
        /// </summary>
        /// <returns>
        /// True if the action succeeds, false if the file does not exists.
        /// </returns>
        public bool DeleteFile(string fileName)
        {
            string uri = ToUri(fileName);
            bool fileExists = System.IO.File.Exists(uri);
            if (fileExists) System.IO.File.Delete(uri);
            return fileExists;
        }
    }
}